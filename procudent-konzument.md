[remark]:<class>(center, middle)

Paralelní programování
======================
Producent – konzument
--------------------------

[remark]:<slide>(new)
## Producent – konzument

* více producentů
* více konzumentů
* sdílená paměť – buffer
* konzument odebírá data z paměti
  * k paměti může přistupovat pouze jeden
  * čtení i zápis je exkluzivní

data = obtainData()
buffer.push(data)

data = buffer.pop
processData(data)


[remark]:<slide>(new)
## Producenti a konzumenti
* Implementováno pomocí objketu *BlockingQueue*

![BlockingQueue](http://tutorials.jenkov.com/images/java-concurrency-utils/blocking-queue.png)

### Metody:
|           | Throws Exception | Special Value | Blocks | Times Out                   |
| --------- | ---------------- | ------------- | ------ | --------------------------- |
| *Insert*  | add(o)           | offer(o)      | put(o) | offer(o, timeout, timeunit) |
| *Remove*  | remove(o)        | poll()        | take() | poll(timeout, timeunit)     |
| *Examine* | element()        | peek()        |        |                             |

[remark]:<slide>(new)

* Základní implemntace:
    * `ArrayBlockingQueue`
    * `DelayQueue`
    * `LinkedBlockingQueue`
    * `PriorityBlockingQueue`
    * `SynchronousQueue`

### Příklad základ:
```java
public class BlockingQueueExample {

    public static void main(String[] args) throws Exception {

        BlockingQueue queue = new ArrayBlockingQueue(1024);

        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);

        new Thread(producer).start();
        new Thread(consumer).start();

        Thread.sleep(4000);
    }
}
```

[remark]:<slide>(new)
### Příklad BlockingQueue: _Producent_
```java
public class Producer implements Runnable {

    protected BlockingQueue queue = null;

    public Producer(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            queue.put("1");
            Thread.sleep(1000);
            queue.put("2");
            Thread.sleep(1000);
            queue.put("3");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

[remark]:<slide>(new)
### Příklad BlockingQueue: _Konzument_
```java
public class Consumer implements Runnable {

    protected BlockingQueue queue = null;

    public Consumer(BlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            System.out.println(queue.take());
            System.out.println(queue.take());
            System.out.println(queue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
````
