[remark]:<class>(center, middle)
# Síťové operace s NIO

Navazuje na [New I/O](/topic/java.nio) 

[remark]:<slide>(new)
## SocketChannel
* SocketChannel má stejné využití jako třída `java.net.Socket`. 

* Reprezentuje soket klienta připojeného k serverové aplikaci protokolem TCP/IP. 

### Základní metody pro práci
#### `static SocketChannel open(SocketAddress remote)`	
* Statická tovární metoda třídy SocketChannel. 
* Otevře kanál soketu, připojí ho k zadané adrese a vrátí na něj odkaz. Pokud bychom použili metodu open() bez parametru, museli bychom soket dodatečně připojit voláním metody connect(Socke­tAddress remote).

[remark]:<slide>(new)
#### `void close()`	
* Uzavře kanál. 
* Pokus o jakoukoliv další I/O operaci vyhodí chybu `java.nio.channels.ClosedChannelException`.

[remark]:<slide>(wait)
#### `int read(ByteBuffer dst)`	
* Načte data do bajtového bufferu. 
* Protože tato metoda nemusí vždy zaplnit celý buffer (resp. zbytek bufferu), vrací počet přečtených bajtů. 
* Pokud vrátí hodnotu –1, kanál byl uzavřen.

[remark]:<slide>(wait)
#### `int write(ByteBuffer src)`
* Pokusí se zapsat do všechny kanálu bajty od aktuální pozice bufferu až do limitu. 
* Protože nemusí všechna data odeslat, vrátí počet zapsaných bajtů.

[remark]:<slide>(new)
### HTTP Download
* Ukázkový příklad zvolil HTTP „stahovač“. 

* Jedná se o velice primitivní obdobu známého wgetu, která téměř nic neumí :-). Je však dobrou ukázkou typické práce s NIO na straně klienta.

* Jako parametr programu zadáme URL souboru, které chceme stáhnout. 
  - Program pomocí java.net.URL z URL získá adresu serveru, port a jméno souboru. 

* Všechna data bez jakékoliv změny nebo kontroly přepošleme do souborového kanálu, a to včetně HTTP hlaviček.

#### Notes
Jako první načteme z parametrů port. 
Dále vytvoříme adresu serveru, připojíme soket a vytvoříme soubor, do kterého budeme zapisovat. 
Se souborem budeme pracovat také pomocí NIO. 
Kanál pro zápis získáme voláním metody FileOutputStream.getChannel(). 
Dále vytvoříme přímý bajtový buffer metodou ByteBuffer.allocateDirect(). 
Posledním krokem před začátkem síťové komunikace bude sestavení HTTP požadavku.

Ještě před zápisem převedeme řetězec požadavku. 
Následně v cyklu zapíšeme do bufferu 256 bajtů (nebo méně pokud jich tolik nezbylo). 
Obsah bufferu se budeme do soketu pokoušet zapsat tak dlouho, dokud se neodešle úplně celý. 
Po dokončení přejdeme na další průchod cyklu, abychom mohli poslat další data.

Čtení ze soketu probíhá podobně. Načteme ze soketu nějaké bajty a všechny je pak zapíšeme do souboru.

Program končí přechodem do finally bloku, kde uzavřeme všechny systémové prostředky, tedy soubor, jeho kanál a síťový soket.

[remark]:<slide>(new)
```java
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;

/** Hlavní třída HTTP stahovače. */
public class HttpDownload {

    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("Použití: java HttpDownload URL");
            System.exit(-1);
        }

        URL url = null;
        try {
            url = new URL(args[0]); //načíst URL
        }
        catch(MalformedURLException e) {
            System.out.println("Neplatné URL.");
            System.exit(-1);
        }
        //vytvořit adresu pro připojení soketu
        SocketAddress addr = new InetSocketAddress(url.getHost(), url.getPort());

        SocketChannel socket = null;
        FileOutputStream fos = null;
        FileChannel file = null;

        try {
            //vytvořit a otevřít soket
            socket = SocketChannel.open(addr);

            //vytvořit soubor a souborový kanál
            String filename = url.getFile().substring(url.getFile().lastIndexOf('/') + 1);
            File f = new File(filename);
            f.createNewFile(); //vytvořit nový soubor
            fos = new FileOutputStream(f); //vytvořit výstupní proud
            file = fos.getChannel(); //ze vstupního proudu získat kanál pro zápis

            //vytvořit buffer
            ByteBuffer buffer = ByteBuffer.allocateDirect(256);

            //sestavit HTTP požadavek
            String http = "GET " + url.getFile() + " HTTP/1.0\n"
                             + "Host: " + url.getHost() + "\n\n";

            //zapsat do kanálu HTTP požadavek
            int nbytes = 0;
            byte[] bytes = http.getBytes(); //převést HTTP požadavek na pole bajtů
            while(nbytes < bytes.length) { //opakovat cyklus, dokud je co zapisovat
                int len = Math.min(buffer.capacity(), bytes.length - nbytes);
                buffer.put(bytes, nbytes, len); //vložit bajty do bufferu
                buffer.flip();
                nbytes += len;

                int n = 0;
                while(n < len) { //zapsat všechny bajty do soketu
                    n += socket.write(buffer);
                }
            }

            while(true) { //pořád číst data ze soketu
                int read = socket.read(buffer);
                if(read == -1) break; //signál uzavření soketu
                buffer.flip();

                int n = 0;
                while(n < read) { //zapsat všechny bajty do souboru
                    n += file.write(buffer);
                }
            }
        }
        catch(IOException e) {
            e.printStackTrace(System.err);
        }
        finally {
            try {
                if(socket != null) socket.close();
                if(fos != null) fos.close();
                if(file != null) file.close();
            }
            catch(IOException e) {}
        }
    }
} 
```

[remark]:<slide>(new)
## Třída ServerSocketChannel
* Představuje kanál na straně serveru

* Kanál otevřeme pomocí metody `ServerSocketChannel.open()`. 

* Jeho serverový soket však ještě nikde nenaslouchá. 
  - Proto musíme pomocí metody `socket()` získat ServerSocket a zavolat jeho metodu `bind()`.

```java
ServerSocketChannel ssc = ServerSocketChannel.open();
ssc.socket().bind(new InetSocketAddress("localhost", 80)); 
```

* Dále je vhodné nastavit, zda bude serverový kanál v neblokujícím módu. 
  - Výběr režimu mezi blokujícím a neblokujícím má totiž zásadní vliv na další používání kanálu.

```java
ssc.configureBlocking(false); //přepne do neblokujícího režimu 
```

[remark]:<slide>(new)
* Přijetí nového již není zajišťováno pomocí `accept()` ze třídy `ServerSocket`, nýbrž stejnojmennou metodou z ServerSocketChannel. 

* Pokud je serverový kanál v **blokujícím režimu**, bude se `accept()` chovat stejně jako ta ze `ServerSocket`. 
  - Zastaví běh aktuálního vlákna, dokud se nepřipojí nějaký klient. 
  - Vlákno uvede do chodu vrácením odkazu na jeho soket. 
  
* V *neblokujícím módu* `accept()` vrátí odkaz na soket nového klienta, nebo `null`. 
  - Vlákno ale nikdy nepozastaví.

* Po ukončení práce uzavřeme serverový soket kanálu i kanál samotný.

```java
ssc.socket().close();
ssc.close();
```

[remark]:<slide>(new)
### Selektory
* Nyní se dostáváme k jedné z obtížnějších částí New I/O – k selektorům. 
 
* Zjednodušeně řečeno, selektor je množina několika kanálů, u které můžeme sledovat, který prvek je aktivní. 
  - V ServerSocket vyžaduje pro každého připojeného klienta jsme vytvořili jedno vlákno. 
  - To čekalo, až od klienta přijdou nějaká data. 

* Co kdybychom všechny klientské sokety „zabalili“ do selektoru? 
  - Měli bychom pouze jedno vlákno, které by čekalo na aktivitu celého selektoru. 
  - Pak bychom vybrali pouze kanály, do kterých přišla nějaká data. 

* Této technice se říká multiplexování a je nejvýznaměnším rozšířením oproti klasickému java.io/java.net API.

[remark]:<slide>(new)
####  Vytvoření selektoru
* Selektor vytvoříme pomocí metody open() třídy Selector z balíku java.nio.channels.

```java
Selector sel = Selector.open();
```

[remark]:<slide>(new)
####  Registrace kanálů
* Kanál přepnutý do neblokujícího režimu do selektoru zaregistrujeme pomocí metody `register()` tohoto kanálu. 

* Tato metoda pochází ze třídy `SelectableChannel`, takže pokud ji kanál nemá, selektory nepodporuje. 
  - Jako první parametr očekává odkaz na příslušný selektor. 
  - Druhý parametr je množina operací, které budou selektor zajímat. 
  - Třetím parametrem můžeme kanálu zaregistrovanému libovolný objekt.

```java
SelectionKey key = channel.register(sel, SelectionKey.OP_READ);
```

* Tento příkaz zaregistruje k selektoru sel kanál channel tak, aby selektor zajímala pouze příchozí data. 
  - Zároveň vrátí odkaz na prvek `SelectionKey`, který umožňuje spravovat připojení k selektoru.
  - Lze pomocí něj kanál ze selektoru odstranit, získat přílohu, připojit jinou přílohu atd.

```java
Object o = key.attachment(); //získáme přílohu
key.attach(new Object());    //připojíme jinou přílohu
key.cancel();                //zrušíme registraci u selektoru 
```

[remark]:<slide>(new)
####  Výběr aktivních kanálů
* K výběru aktivních kanálů slouží tyto tři metody:
  - `select()` – Zastaví aktuální vlákno, dokud nevykazuje alespoň jeden z registrovaných kanálů nějakou aktivitu.
  - `select(long timeout)` – Stejné jako předchozí, ovšem blokuje vlákno pouze po dobu timeout milisekund.
  - `selectNow()` – Okamžitě se vrací bez ohledu na to, kolik kanálů je aktivních.

* Metoda `select()` vrací celé číslo, které určuje počet aktivních kanálů. 

* Množinu `Set<SelectionKey>` s aktivními kanály pak vrací metoda `selectedKeys()`.

* Kanál získáme z prvku SelectionKey metodou channel(). 
  - Za zmínku také stojí metoda selektoru `wakeup()`, která dokáže probudit vlákno zablokované metodou `select()`.

[remark]:<slide>(wait)
####  Uzavření selektoru
* Protože selektor je stejně jako např. soket prostředek operačního systému, musíme ho uzavřít metodou `close()`.


[remark]:<slide>(new)
###  HTTP server
* Server bez ohledu na obsah požadavku pošle text/plain dokument s obsahem „Simple HTTP Server“. 

* Na příkladu si však ukážeme práci se serverovým kanálem, selektory a jeden postup, jak převést obsah bajtového bufferu na řetězec.

[remark]:<slide>(new)
#### Spuštění serveru
* Vytvoříme serverový kanál, přepneme ho do neblokujícího režimu a připojíme jeho soket na všechna síťová rozhraní.
 
* Také otevřeme selektor a z třídy java.nio.charset.Charset získáme dekodér pro ASCII tabulku. 

* Zároveň selektoru zaregistrujeme serverový kanál. Zajímat nás bude přijetí nového klienta čili SelectionKey.OP_ACCEPT.

```java
ssc = ServerSocketChannel.open();
ssc.configureBlocking(false);
selector = Selector.open();
ssc.register(selector, SelectionKey.OP_ACCEPT);
ssc.socket().bind(new InetSocketAddress("0.0.0.0", PORT));
decoder = Charset.forName("US-ASCII").newDecoder();
buffer = ByteBuffer.allocateDirect(256); 
```

[remark]:<slide>(new)
#### Hlavní smyčka serveru
* Budeme pokračovat cyklem `while`, který bude trvat, dokud bude serverový kanál otevřený. 

* Zavoláme metodu `select()` selektoru, která zablokuje hlavní (a jediné) vlákno aplikace. 

* Znovu ho uvede do běhu, jakmile přijdou nějaká data do zaregistrovaných soketů, nebo se k serveru připojí nový klient.

* Prvky `SelectionKey` vrácené metodou `selector.selectedKeys()` projdeme pomocí iterátoru. 
  - Pokud bude kanál vrácený metodou `channel()` prvku `SelectionKey` serverový kanál, víme, že se připojil nový klient. 
  - Jediná aktivita, pro kterou jsme serverový kanál registrovali k selektoru, je totiž OP_ACCEPT.

[remark]:<slide>(new)
* U kanálu nově připojeného klienta zapneme neblokující režim. 
  - Pak ho zaregistrujeme do selektoru. 
  - Jako přílohu připojíme nový objekt typu `StringBuffer`, do kterého budeme ukládat HTTP požadavek.

* Jestliže se nejednalo o serverový kanál, můžeme s jistotou říct, že do nějakého soketu přišla část HTTP požadavku. 
  - Obsah bufferu převedeme na řetězec a připojíme na konec `StringBufferu`. 
  - V tom se následně pokusíme vyhledat dvojici CRLF znaků, abychom zjistili, jestli dorazil celý HTTP požadavek. 
  - Pokud již dorazil, zapíšeme do soketu odpověď a ukončíme spojení.

* Samotný převod na řetězec probíhá pomocí objektu decoder typu java.nio.charset.CharsetDecoder a jeho metody `decode()`. 
  - Ta očekává jako parametr bajtový buffer a vrací znakový buffer, který jednoduše pomocí metody `toString()` převedeme na řetězec.

[remark]:<slide>(new)
```java
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.util.*;

/** Hlavní třída jednoduchého HTTP Serveru. */
public class SimpleHTTPServer {

    public static final int PORT = 10997;

    /** Univerzální odpoveď */
    public static final String RESPONSE = "HTTP/1.0 200 OK\r\n"
        + "Connection: closed\r\n"
        + "Content-Type: text/plain\r\n"
        + "Content-length: 20\r\n"
        + "\r\n"
        + "Simple HTTP server.\n";

    private ServerSocketChannel ssc;
    private Selector selector;
    private CharsetDecoder decoder;
    private ByteBuffer buffer;

    public void run() {
        try {
            ssc = ServerSocketChannel.open(); //otevřít serverový kanál
            ssc.configureBlocking(false); //vypnout blokování
            selector = Selector.open(); //otevřít selektor
            ssc.register(selector, SelectionKey.OP_ACCEPT); //přidat kanál do selektoru
            ssc.socket().bind(new InetSocketAddress("0.0.0.0", PORT)); //připojit serverový soket
            decoder = Charset.forName("US-ASCII").newDecoder(); //vytvořit dekóder pro ASCII
            buffer = ByteBuffer.allocateDirect(256); //alokovat bajtový buffer

            while(ssc.isOpen()) {
                selector.select(); //zablokovat vlákno, dokud není selektor aktivní
                Set<SelectionKey> keys = selector.selectedKeys(); //získat vybrané prvky SelectionKey
                Iterator<SelectionKey> i = keys.iterator();
                while(i.hasNext()) { //projít množinu
                    SelectionKey key = i.next();
                    i.remove();
                    Channel chan = key.channel(); //získat kanál
                    if(chan == ssc) { //pokud je to serverový kanál, připojil se nový klient
                        SocketChannel client = ssc.accept(); //přijmout klienta
                        if(client != null) { //pokud se někdo opravdu připojil
                            client.configureBlocking(false); //vypnout blokování
                            client.register(selector, SelectionKey.OP_READ, new StringBuffer()); //a přidat do selektoru
                        }
                    }
                    else { //aktivní je kanál nějakého z klientů
                        SocketChannel client = (SocketChannel)chan;
                        int read = client.read(buffer); //přečíst data
                        if(read == -1) { //ukončeno spojení
                            key.cancel(); //vyřadit ze selektoru
                            client.close(); //zavřít kanál
                            continue;
                        }
                        StringBuffer sb = (StringBuffer)key.attachment(); //získat HTTP požadavek v příloze
                        buffer.flip();
                        sb.append(decoder.decode(buffer).toString()); //připojit k němu obsah bufferu
                        buffer.clear();

                        if(sb.indexOf("\r\n\r\n") != -1) { //přišel celý HTTP požadavek
                            buffer.put(RESPONSE.getBytes()); //vložit odpoveď do bufferu
                            buffer.flip();

                            int written = 0;
                            while(written < RESPONSE.length()) {
                                written += client.write(buffer); //odeslat
                            }
                            buffer.clear();
                            key.cancel(); //vyřadit ze selektoru
                            client.close(); //zavřít kanál
                        }
                    }
                }
            }
        }
        catch(IOException e) {
            e.printStackTrace(System.err);
        }
        finally {
            try {
                if(ssc != null) {
                    ssc.socket().close();
                    ssc.close();
                }
                if(selector != null) selector.close();
            }
            catch(IOException e) {}
        }
    }

    public static void main(String[] args) {
        new SimpleHTTPServer().run();
    }
}
```