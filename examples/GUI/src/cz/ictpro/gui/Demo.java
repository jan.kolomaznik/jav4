package cz.ictpro.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Demo {
    private JButton button1;
    private JPanel root;
    private JLabel status;
    private JTextPane result;

    public Demo() {
        button1.addActionListener(this::work);
    }

    public JPanel getRoot() {
        return root;
    }

    public void work(ActionEvent e) {
        BSwingWorker swingWorker = new BSwingWorker(status, result);
        swingWorker.execute();
        status.setText(swingWorker.getState().toString());
    }

}
