package cz.ictpro.gui;

import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class BSwingWorker extends SwingWorker<String, Integer> {

    private JLabel status;
    private JTextPane result;

    public BSwingWorker(JLabel status, JTextPane result) {
        this.status = status;
        this.result = result;
    }

    @Override
    protected String doInBackground() throws Exception {
        SwingUtilities.invokeLater(() -> {
            status.setText(getState().toString());
        });
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 1000; i++) {
            sb.append(i).append("\n");
            publish(i);
            Thread.sleep( 10);
        }
        return sb.toString();
    }

    @Override
    protected void process(List<Integer> chunks) {
        status.setText(String.format(
                "Status: %s, chunks: %d, progress %d",
                getState(),
                chunks.size(),
                chunks.get(0)
        ));
    }

    @Override
    protected void done() {
        try {
            status.setText(getState().toString());
            result.setText(get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


}
