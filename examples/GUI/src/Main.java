import cz.ictpro.gui.Demo;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        Demo demo = new Demo();

        SwingUtilities.invokeAndWait(() -> {
            JFrame mainFrame = new JFrame("title");
            mainFrame.setContentPane(demo.getRoot());
            mainFrame.setTitle("Demo GUI Thread");
            mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            mainFrame.pack();
            mainFrame.setVisible(true);
        });

    }
}
