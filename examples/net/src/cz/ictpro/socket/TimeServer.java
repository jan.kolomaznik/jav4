package cz.ictpro.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeServer {

    public static void main(String[] args) throws Exception {

        ServerSocket serverSocket = new ServerSocket(9000);
        while (true) {
            Socket client = serverSocket.accept();
            new Thread(() -> {
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    PrintStream out = new PrintStream(client.getOutputStream());

                    out.println("Send mi time format");
                    out.flush();

                    String format = br.readLine();
                    SimpleDateFormat sdf = new SimpleDateFormat(format);

                    out.println(sdf.format(new Date()));
                    out.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        client.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }).start();
        }
    }
}
