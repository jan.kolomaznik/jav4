package cz.ictpro.vlakna;

public class Main {

    private static String value = "A";


    static class MyThread extends Thread {

        @Override
        public void run() {
            todo();
        }
    }

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.setPriority(Thread.MAX_PRIORITY);
        myThread.start();
        System.out.println(myThread.isAlive());

        Thread threadRunnable = new Thread(new Runnable() {
            @Override
            public void run() {
               todo();
            }
        });
        threadRunnable.start();

        Thread threadLambda = new Thread(Main::todo, "threadLambda");
        threadLambda.start();
    }

    public static void todo() {
        synchronized (value) {
            value = value + "A";
            System.out.println(
                    "Thread name: " +
                            Thread.currentThread().getName() +
                            ", value :" + value
            );
        }
    }
}
