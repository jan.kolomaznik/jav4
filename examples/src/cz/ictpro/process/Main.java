package cz.ictpro.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
	    ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", "dir");
	    pb.directory(new File("C:/"));
	    Process p = pb.start();

        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        p.getInputStream(), "852"));

        br.lines().forEach(System.out::println);

	    p.waitFor();
    }
}
