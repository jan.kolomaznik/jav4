import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

public class Entita {

    //private final static AtomicLong nextId = new AtomicLong();
    private static long nextId = 0;
    private static Semaphore semaphore = new Semaphore(0);

    private final long id;

    public Entita() throws InterruptedException {
        //id = nextId.incrementAndGet();
        semaphore.acquire();
        id = nextId++;
        semaphore.release();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entita entita = (Entita) o;
        return id == entita.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
