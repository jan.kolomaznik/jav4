import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

public class BlockingQueueExample {

    static AtomicLong series = new AtomicLong();

    static class Producer implements Runnable {

        private BlockingQueue queue = null;

        public Producer(BlockingQueue queue) {
            this.queue = queue;
        }

        public void run() {
            Thread current = Thread.currentThread();
            try {
                while (!current.isInterrupted()) {
                    Waits.work(1000);
                    long data = series.incrementAndGet();
                    queue.put(Long.toString(data));
                    System.out.println(current.getName() + " < " + data);
                    Waits.sleep(250);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Consumer implements Runnable {

        protected BlockingQueue<String> queue = null;

        public Consumer(BlockingQueue queue) {
            this.queue = queue;
        }

        public void run() {
            Thread current = Thread.currentThread();
            try {
                while (!current.isInterrupted()) {
                    String msg = queue.take();
                    Waits.work(500);
                    System.out.println(current.getName() + " > " + msg);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Thread.sleep(4000);

        BlockingQueue queue = new ArrayBlockingQueue(32);
        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);

        new Thread(producer, "producer-0").start();
        //new Thread(producer, "producer-1").start();
        //new Thread(producer, "producer-2").start();
        //new Thread(producer, "producer-3").start();
        //new Thread(producer, "producer-4").start();
        new Thread(consumer, "consumer-0").start();
        //new Thread(consumer, "consumer-1").start();

        while (true) {
            Waits.sleep(1000);
            System.out.println("BlockingQueue: " + queue.size());
        }

    }


}
