import javafx.concurrent.Worker;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CtenarPisar {

    static class Point {

        private ReadWriteLock lock = new ReentrantReadWriteLock();
        private double x, y;


        public void setX(double x) {
            lock.writeLock().lock();
            this.x = -x;
            Waits.work(1000);
            this.x = x;
            lock.writeLock().unlock();
        }

        public void setY(double y) {
            lock.writeLock().lock();
            this.y = -y;
            Waits.work(1000);
            this.y = y;
            lock.writeLock().unlock();
        }

        @Override
        public String toString() {
            try {
                lock.readLock().lock();
                Waits.work(1000);
                return "Point{" +
                        "x=" + x +
                        ", y=" + y +
                        '}';
            } finally {
                lock.readLock().unlock();
            }
        }
    }

    static Point point = new Point();
    static AtomicLong series = new AtomicLong();

    public static void main(String[] args) {
        Waits.sleep(5000);
        new Thread(CtenarPisar::pisar, "Pisar-0").start();
        new Thread(CtenarPisar::pisar, "Pisar-1").start();
        new Thread(CtenarPisar::ctenar, "Ctenar-0").start();
        new Thread(CtenarPisar::ctenar, "Ctenar-1").start();
        new Thread(CtenarPisar::ctenar, "Ctenar-2").start();


    }

    private static Random generator = new Random();

    public static void pisar() {
        while (true) {
            Waits.sleep(2000 * generator.nextInt(5));
            point.setX(series.incrementAndGet());
            Waits.sleep(2000 * generator.nextInt(5));
            point.setY(series.incrementAndGet());
        }
    }

    private static void ctenar() {
        while (true) {
            Waits.sleep(500 * generator.nextInt(5));
            System.out.println(point.toString());
        }
    }
}
