public class Sync {

    static class Person {
        private String name;
        private int age;

        public synchronized void setName(String name) {
            Waits.work(3000);
            this.name = name;
        }

        public synchronized void setAge(int age) {
            Waits.work(4000);
            this.age = age;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static void main(String[] args) {
        Waits.sleep(10_000);
        Person p = new Person();
        asynSetName(p, "Pepa");
        asynSetAge(p, 15);
        for (int i = 0; i < 4; i++) {
            synchronized ("Polis 2") {
                Waits.work(2000);
                System.out.println(p.toString());
            }
            Waits.sleep(1000);
        }

    }

    public static void asynSetName(Person person, String name) {
        new Thread(() -> {
            person.setName(name);
        }).start();
    }

    public static void asynSetAge(Person person, int age) {
        new Thread(() -> {
            person.setAge(age);
        }).start();
    }
}
