import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Main {

    static class Generator extends Thread {

        private List<Entita> entitas = new LinkedList<>();

        @Override
        public void run() {
            long starTime = System.currentTimeMillis();
            for (int i = 0; i < 10_000; i++) {
                try {
                    entitas.add(new Entita());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println((System.currentTimeMillis() - starTime) / 100);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Generator[] generators = new Generator[10];
        for (int i = 0; i < generators.length; i++) {
            generators[i] = new Generator();
        }
        System.out.println("Created");
        for (Generator generator: generators) {
            generator.start();
        }
        for (Generator generator: generators) {
            generator.join();
        }
        System.out.println("Generated");


        Set<Entita> entitas = new HashSet<>();
        for (Generator generator: generators) {
            for (Entita entita: generator.entitas) {
                if (entitas.contains(entita)) {
                    System.out.println("BINGO (" + entitas.size() + ")");
                    return;
                } else {
                    entitas.add(entita);
                }
            }
        }
        System.out.println("Nothing");
    }
}
