public class Waits {

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }

    public static void work(long time) {
        long start = System.currentTimeMillis();
        while ((System.currentTimeMillis() - start) < time) {
            // WORK
        }
    }
}
