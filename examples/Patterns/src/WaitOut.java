public class WaitOut {

    public static void main(String[] args) throws InterruptedException {

        String monitor = "monitor";
        synchronized (monitor) {
            monitor.wait(10_000);
        }
        System.out.println("Done");

    }
}
