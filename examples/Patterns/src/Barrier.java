import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Barrier {

    static class CyclicBarrierRunnable implements Runnable{

        CyclicBarrier barrier1 = null;
        CyclicBarrier barrier2 = null;
        long time;

        public CyclicBarrierRunnable(
                CyclicBarrier barrier1,
                CyclicBarrier barrier2,
                long time) {

            this.barrier1 = barrier1;
            this.barrier2 = barrier2;
            this.time = time;
        }

        public void run() {
            try {
                Waits.work(time);
                System.out.println(Thread.currentThread().getName() +
                        " waiting at barrier 1");
                this.barrier1.await();

                Waits.work(time);
                System.out.println(Thread.currentThread().getName() +
                        " waiting at barrier 2");
                this.barrier2.await();

                Waits.work(time);
                System.out.println(Thread.currentThread().getName() +
                        " done!");

            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        Runnable barrier1Action = () -> {
            Waits.work(2500);
            System.out.println("BarrierAction 1 executed ");
        };
        Runnable barrier2Action = () -> {
            Waits.work(2500);
            System.out.println("BarrierAction 2 executed ");
        };

        CyclicBarrier barrier1 = new CyclicBarrier(2, barrier1Action);
        CyclicBarrier barrier2 = new CyclicBarrier(2, barrier2Action);

        CyclicBarrierRunnable barrierRunnable1 =
                new CyclicBarrierRunnable(barrier1, barrier2, 2000);

        CyclicBarrierRunnable barrierRunnable2 =
                new CyclicBarrierRunnable(barrier1, barrier2, 2500);

        new Thread(barrierRunnable1, "barrierRunnable1").start();
        new Thread(barrierRunnable2, "barrierRunnable2").start();

    }

}
