import java.util.Date;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class Planovani {

    static final AtomicLong atomicLong = new AtomicLong();

    static Runnable task(String name, long time) {
        return () -> {
            try {
                System.out.printf("Task %s start at %s.%n", name, new Date().toString());
                TimeUnit.SECONDS.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.printf("Task %s done %d.%n", name, atomicLong.incrementAndGet());
            }
        };
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ScheduledFuture scheduledFuture = ses
                .scheduleWithFixedDelay(
                        task("schedule 1", 1),
                        5,
                        10,
                        TimeUnit.SECONDS);

        TimeUnit.MINUTES.sleep(1);
        scheduledFuture.cancel(false);
    }
}
