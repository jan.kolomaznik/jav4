import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class InvokAll {

    static final AtomicLong atomicLong = new AtomicLong();

    static Callable<Long> task(String name, long time) {
        return () -> {
            try {
                System.out.printf("Task %s start.%n", name);
                TimeUnit.SECONDS.sleep(time);
                return atomicLong.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            } finally {
                System.out.printf("Task %s done.%n", name);
            }
        };
    }


    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService executorService = Executors.newWorkStealingPool();
        Long result = executorService.invokeAny(
                Arrays.asList(
                        task("A", 5),
                        task("B", 4),
                        task("C", 3),
                        task("D", 2),
                        task("E", 1))
        );

        System.out.println(result);
    }
}
