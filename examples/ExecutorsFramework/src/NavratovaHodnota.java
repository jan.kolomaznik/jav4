import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

public class NavratovaHodnota {

    static final AtomicLong atomicLong = new AtomicLong();

    static Callable<Long> task = () -> {
        try {
            TimeUnit.SECONDS.sleep(1);
            return atomicLong.incrementAndGet();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    };

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Long> future = executorService.submit(task);


        System.out.println("isDone: " + future.isDone());
        try {
            System.out.println("get: " + future.get(500, TimeUnit.MILLISECONDS));
        } catch (TimeoutException e) {
            e.printStackTrace();
            executorService.shutdownNow();
        }
        System.out.println("isDone: " + future.isDone());
        System.out.println("get: " + future.get());
    }
}
