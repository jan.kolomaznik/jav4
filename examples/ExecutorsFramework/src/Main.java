import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    static AtomicLong series = new AtomicLong();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newWorkStealingPool();
        for (int i = 0; i < 1_000_000; i++) {
            executorService.submit(Main::task);
        }
        System.out.println("MAIN DONE");
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.SECONDS);


    }


    public static void task() {
        Thread current = Thread.currentThread();
        System.out.println(current.getName() + "> " + series.incrementAndGet());
    }
}
